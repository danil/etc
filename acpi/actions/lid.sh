#!/bin/bash
# # Danil.
# # The script uses the hibernate-ram command from sys-power/hibernate-script
# # make sure the package is installed on your system
# # <https://wiki.gentoo.org/wiki/ACPI>.

# if [ -x "/usr/sbin/hibernate-ram" ]
# then
#     /usr/sbin/hibernate-ram
# else
#     logger "${0}: hibernate-ram command does not exist. Aborting."
#     exit 1
# fi
