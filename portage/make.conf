# -*- coding: utf-8; -*-
# These settings were set by the catalyst build script that automatically
# built this stage.
# Please consult /usr/share/portage/config/make.conf.example for a more
# detailed example.

# Danil <https://wiki.gentoo.org/wiki//etc/portage/make.conf>.
# The CFLAGS and CXXFLAGS variables define the optimization flags for
# the gcc C and C++ compiler respectively.
# <http://gentoo.org/doc/en/handbook/handbook-amd64.xml?part=1&chap=5&style=printable#doc_chap4_sect2>,
# <http://gentoo.org/doc/en/gcc-optimization.xml?style=printable>,
# <http://en.gentoo-wiki.com/wiki/Safe_Cflags>,
# <http://en.gentoo-wiki.com/wiki/Safe_Cflags/Intel>,
# <http://en.gentoo-wiki.com/wiki/Lenovo_ThinkPad_T400#Installation_Prep>,
# <http://www.linode.com/wiki/index.php/Gentoo#Switch_profiles>,
# <http://www.laozhu.me/2013/04/install-gentoo-on-linode/>.
# Examining CPU information `cat /proc/cpuinfo`.
# For intel haswel i7 GCC 4.8 users can set -march=core-avx2
# <http://wiki.gentoo.org/wiki/Lenovo_ThinkPad_T440s#Compiler_flags>
CFLAGS="-O2 -pipe"
CXXFLAGS="${CFLAGS}"

# WARNING: Changing your CHOST is not something that should be done lightly.
# Please consult <http://gentoo.org/doc/en/change-chost.xml?style=printable>
# before changing.

# Examining CPU information `grep flags /proc/cpuinfo`.
CPU_FLAGS_X86="aes apic arat arch_perfmon avx clflush cmov constant_tsc cx16 cx8 de eagerfpu erms f16c fpu fsgsbase fxsr hypervisor lahf_lm lm mca mce mmx msr mtrr nopl nx pae pat pcid pclmulqdq pdpe1gb pge pni popcnt pse pse36 rdrand rdtscp rep_good sep smep ss sse sse2 sse4_1 sse4_2 ssse3 syscall tsc tsc_adjust tsc_deadline_timer vme x2apic xsave xsaveopt"

CHOST="x86_64-pc-linux-gnu"
# How many parallel compilations should occur when you install a
# package? Usually, the number of processors + 1 is a good value.
# <http://gentoo.org/doc/en/handbook/handbook-amd64.xml?part=1&chap=5&style=printable#doc_chap4_sect3>,
# <http://gentoo.org/doc/en/gentoo-x86-quickinstall.xml?style=printable#doc_chap2_pre33>,
# <http://unix.stackexchange.com/questions/51815/why-people-recommend-the-j3-option-for-make-when-having-a-dual-core-cpu#51861>,
# <http://en.gentoo-wiki.com/wiki/Lenovo_ThinkPad_T400#Installation_Prep>.
MAKEOPTS="-j1"

# Portage's Configuration File Protection
# <http://gentoo.org/doc/en/handbook/handbook-amd64.xml?part=3&chap=2&style=printable#doc_chap3>,
# man emerge(1) CONFIGURATION FILES.
CONFIG_PROTECT="/boot /home /usr/lib/urxvt/perl"
CONFIG_PROTECT_MASK="-/etc/env.d"

# Caching compilation intermediate results and fetch the source files
# for the next package in the list even while it is compiling another
# package
# <http://gentoo.org/doc/en/handbook/handbook-amd64.xml?part=2&chap=3&style=printable#doc_chap3>,
# <http://gentoo.org/doc/en/handbook/handbook-amd64.xml?part=2&chap=3&style=printable#doc_chap5_sect1>.
FEATURES="ccache parallel-fetch"
CCACHE_SIZE="4G"

# Set PORTDIR for backward compatibility with various tools:
#   gentoo-bashcomp - bug #478444
#   euse - bug #474574
#   euses and ufed - bug #478318
PORTDIR="/usr/portage"

DISTDIR="${PORTDIR}/distfiles"
PKGDIR="${PORTDIR}/packages"

# Prefered nearby mirrors
# <http://gentoo.org/main/en/mirrors2.xml#Russia>,
# <http://gentoo.org/doc/en/gentoo-x86-quickinstall.xml?style=printable#doc_chap2_sect17>.
GENTOO_MIRRORS="http://mirror.yandex.ru/gentoo-distfiles/ ${GENTOO_MIRRORS}"
GENTOO_MIRRORS="ftp://mirror.yandex.ru/gentoo-distfiles/ ${GENTOO_MIRRORS}"

# Gentoo Overlays.
# Layman
# <http://gentoo.org/proj/en/overlays/userguide.xml?style=printable#doc_chap2_pre2>,
# <http://en.gentoo-wiki.com/wiki/Overlay#Using_Overlays_with_Portage_-_Layman>,
# Sunrise <http://overlays.gentoo.org/proj/sunrise>.
source /var/lib/layman/make.conf

# Display server VIDEO_CARDS is an alias for x11-drivers/xf86-video-* packages
# <http://gentoo.org/doc/en/xorg-config.xml?style=printable#doc_chap2>,
# <http://wiki.gentoo.org/wiki/Xorg/Configuration#make.conf_configuration>,
# <http://wiki.gentoo.org/wiki/Intel>,
# <http://wiki.gentoo.org/wiki/Nouveau>.
VIDEO_CARDS="intel vesa"

# Display server INPUT_DEVICES is an alias for the x11-drivers/xf86-input-* packages
# <http://gentoo.org/doc/en/xorg-config.xml?style=printable#doc_chap2>,
# <http://en.gentoo-wiki.com/wiki/X.Org/Input_drivers#INPUT_DEVICES>.
INPUT_DEVICES="evdev keyboard mouse synaptics"

# LINGUAS affects to localisation files that get installed in
# gettext-based programs, and decides used localisation for some
# specific software packages, such as kde-base/kde-l10n,
# app-office/openoffice and www-client/mozilla-firefox
# <http://gentoo.org/doc/en/guide-localization.xml?style=printable#doc_chap3_pre6>,
# <http://ru.gentoo-wiki.com/wiki/Полная_поддержка_русского_языка#Firefox.2C_OpenOffice_.D0.B8_.D0.BD.D0.B5.D0.BA.D0.BE.D1.82.D0.BE.D1.80.D1.8B.D0.B5_.D0.B4.D1.80.D1.83.D0.B3.D0.B8.D0.B5>.
LINGUAS="en_US en ru_RU ru"
L10N="en-US en ru-RU ru"

# Temporary Portage Files
# <http://gentoo.org/doc/en/handbook/handbook-amd64.xml?part=3&chap=1&style=printable#doc_chap3_sect1>.
#PORTAGE_TMPDIR="/var/tmp"

# USE flags.
# These are the USE flags that were used in addition to what is provided by the
# profile used for building.
# USE="${USE}"
# USE="systemd ${USE}" # instead eselect profile set to default/linux/amd64/nn.n/systemd
USE="icu ${USE}"

# # <https://wiki.gentoo.org/wiki/Python#Version_upgrade>
# PYTHON_TARGETS="python2_7 python3_5"
# PYTHON_SINGLE_TARGET="python3_5

# Xfce
# <http://wiki.gentoo.org/wiki/Xfce#Installation>.
XFCE_PLUGINS=""
